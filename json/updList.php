<?php
if (!defined("ENTRY"))
    exit("Invalid entry point");

try {
    $list = new ulist($json->id);
    if ($list->getId() === false) { // List does not exist
        $output['status'] = 18;
        $output['errmsg'] = "List does not exist";
    } else {
        $list->syncItems($json->items);
        if ($GLOBALS['db']->isFail()) {
            $output['status'] = 17;
            $output['errmsg'] = "Failed Query";
        } else {
            $output['action'] = "update";
            $output['data'] = $list->toJSON(false);
        }
    }
} catch (Exception $e) {
    $output['status'] = 16;
    $output['errmsg'] = $e->getMessage();
    error_log($e->getMessage());
}

//error_log($list->toJSON());

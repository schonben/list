<?php

/* This file needs to be copied or renamed to settings.php */

// Database Settings
$setting['db']['type'] = "pgsql"; // mysql or pgsql
$setting['db']['server'] = "::1";
$setting['db']['port'] = 5432; // 3306 for mysql & 5432 for postgresql
$setting['db']['user'] = "";
$setting['db']['password'] = "";
$setting['db']['name'] = "";

$setting['apath']="";
$setting['title']="";
$setting['schema']="";
$setting['jsonURL']="http://".$_SERVER["HTTP_HOST"]."/json";
$setting['dev']=1;


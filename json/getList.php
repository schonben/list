<?php
if (!defined("ENTRY"))
    exit("Invalid entry point");

$list = new ulist($json->id);

$output['action'] = "update";
$output['data'] = $list->toJSON(false);


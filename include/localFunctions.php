<?php

function classAutoLoader($class) {
    if (!class_exists($class)) {
        $class = strtolower($class);
        $path = $GLOBALS['setting']['apath']."//classes/{$class}.class.php";
        if (file_exists($path)) {
            include $path;
        }
    }
}

function phpErrorHandler($errno, $errstr, $errfile, $errline) {
    error_log($errstr);

    if (ENTRY == "json") {
        throw new Exception($errstr." in ".$errfile." on line ".$errline,$errno);
    } else {
        displayError($errno, $errstr, $errfile, $errline);
        exit();
    }
}
function genericErrorHandler ($error) {
    error_log($error->getMessage());
    displayError($error->getCode(), $error->getMessage(), $error->getFile(), $error->getLine(),$error->getTrace());
}
function displayError($errno, $errstr, $errfile, $errline, $trace=array()) {
    if (empty($GLOBALS['setting']['dev'])) {
        echo "Error";
    } else {
        echo "<h1>Error #".$errno." in ".$errfile." on line ".$errline."</h1><p>".$errstr."</p>";

        foreach ($trace as $item) {
            print_r($item);
            //echo "<p>".$item."</p>";
        }
    }
}


function isEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}
function linkTo ($link) {
    $url = rtrim($GLOBALS['setting']['rpath'],"/")."/".ltrim($link,"/");
    return "/".ltrim($url,"/");
}
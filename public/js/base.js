$(function (){
    $(".error").effect("highlight", {}, 3000);
});

function getTime() {
    return Math.round(Date.now() / 1000);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name)==0)
            return c.substring(name.length,c.length);
    }
    return "";
}
function setCookie(cname,data) {
    var days = 30;
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    document.cookie = cname + "=" + encodeURIComponent(data) + "; expires=" +date.toGMTString() + "; path=/";
}

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/*
 * Helper object creator for sortable list
 */
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

function dynamicSortMultiple() {
    /*
     * save the arguments object as it will be overwritten
     * note that arguments object is an array-like object
     * consisting of the names of the properties to sort by
     */
    var props = arguments;
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        /* try getting a different result from 0 (equal)
         * as long as we have extra properties to compare
         */
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
}
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        if (typeof a[property].value === "undefined") {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        } else {
            var result = (a[property].value < b[property].value) ? -1 : (a[property].value > b[property].value) ? 1 : 0;
        }
        return result * sortOrder;
    }
}

function showAddFields() {
    $("#newlist").removeClass("hidden");
    $("#newbutton").removeClass("hidden");
    $("#addbutton").addClass("hidden");
    $("#newlist").focus();
}

/*
 * Localstorage manipulation functions
 */

function setList(obj) { // Write object into localstorage
    if (obj.id != "") {
        lists = readLists();
        var index = findIndex(obj.id,lists);
        if (index === false) {
            lists.push(obj);
        } else {
            lists[index] = obj;
        }
        writeLists(lists);
    }
}
function getList(id) { // Read back object from localstorage
    if(typeof id === "undefined") { id = listId; }
    var lists = readLists();

    var index = findIndex(id,lists);
    var list = lists[index];
    if (typeof list !== 'undefined') {
        list.id = id;
        return  list;
    }
    // Return empty object.
    return $.parseJSON('{"id":"'+id+'","name":"","description":"","showCat":"","showQty":"","lastsync":"","lastupdate":"","lastsent":"","items":[]}');
}
function readLists() { // Read the data from localStorage
    var retval = new Array();
    var lsLists = localStorage.getItem("lists");
    if (lsLists !== null) {
        $.each($.parseJSON(lsLists), function (index,value) {
            if (value !== null) {
                retval.push(value);
            }
        })
    }
    return retval;
}
function writeLists(lists) {
    localStorage.setItem("lists",JSON.stringify(lists));
}
function deleteList (id) {
    lists = readLists();
    var index = findIndex(id,lists);
    delete lists[index];
    writeLists(lists);
}
function findIndex(needle,haystack,property) { // Return the index of the item with the specific id.
    if(typeof property === "undefined") { property = "id"; }
    // Error is in this function.
    var index = false;
    for (i=0;i<haystack.length;i++) {
        if (haystack[i][property] == needle) {
            index = i;
        }
    }
    return index;
}

function HTMLEncode(str){
    var i = str.length,
        aRet = [];

    while (i--) {
        var iC = str[i].charCodeAt();
        if (iC < 65 || iC > 127 || (iC>90 && iC<97)) {
            aRet[i] = '&#'+iC+';';
        } else {
            aRet[i] = str[i];
        }
    }
    return aRet.join('');
}

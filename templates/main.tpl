<!DOCTYPE html>
<html{if $setting.dev == 0 && $template == "index"} manifest="{'/cache.manifest'|linkTo}"{/if}>
    <head>
        <meta charset="UTF-8">
        <title>{$setting.title}</title>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0'/>
        <link rel="shortcut icon" href="{'/img/list.png'|linkTo}" type="image/png" />
        <link rel="stylesheet" href="{'/css/style.css'|linkTo}" />
        <link rel="stylesheet" href="{'/css/formedit.css'|linkTo}" />
        <link rel="stylesheet"  href="{'/css/fonts.css'|linkTo}" type="text/css">
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" defer="defer"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" defer="defer"></script>
        <script src="{'/js/jquery.ui.touch-punch.min.js'|linkTo}" defer="defer"></script>
        <script src="{'/js/base.js'|linkTo}" defer="defer"></script>
    </head>

    <body>
        <header>
            {if ($user->getId())}
                <nav id="tools">
                    {if ($template != "settings")}
                        <a id="settingsLink" href="{'/settings'|linkTo}">
                            <img id="settings" src="{'/img/settings.png'|linkTo}" alt="settings"/>
                        </a>
                    {/if}
                    <a id="logoutLink" href="{'/logout'|linkTo}">
                        <img id="logout" src="{'/img/logout.png'|linkTo}" alt="logout"/>
                    </a>
                </nav>
            {/if}
            <h1>{$setting.title}</h1>
        </header>
        <section id="page">
            {if $template != ""}
                {include file="{$template}.tpl"}
            {/if}
        </section>
        <footer>

        </footer>
    </body>
</html>
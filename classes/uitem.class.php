<?php

class uItem {
    //use uuid;
    // TODO upgrade to 5.4 or better to use this.

    protected $id;
    protected $list;
    protected $indb = false;
    const VT = ",cat,so,value,qty,done,";
    protected $cat = array("value"=>"","time"=>0);
    protected $so = array("value"=>"","time"=>0);
    protected $value = array("value"=>"","time"=>0);
    protected $qty = array("value"=>"","time"=>0);
    protected $done = array("value"=>0,"time"=>0);

    public function __construct($data=null) {
        if (uuid::isUUID($data)) {
            $this->retrieve($data);
        } elseif (is_object($data)) {
            $this->id = $data->id;
            $this->updItem($data);
        } elseif (is_array($data)) {
            $this->populate($data);
        }  else {
            // Construct an empty item
            $this->id = uuid::getUUID();
        }
    }

    // Getters
    public function getCat() {
        return $this->cat['value'];
    }
    public function getDone() {
        return $this->done['value'];
    }
    public function getValue($item) {
        if (strpos(static::VT,$item) !== false) {
            return empty($this->{$item}['value'])?0:$this->{$item}['value'];
        }
    }
    public function getTime($item) {
        if (strpos(static::VT,$item) !== false) {
            return $this->timeFormat($this->{$item}['time']);
        }
    }


    // Setters
    public function setList($list) {
        $this->list = $list;
    }

    public function retrieve ($id) {
        $sql = "
            SELECT
              *
            FROM
              #schema#.items
            WHERE
              id = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":list"=>$this->id));
        if ($query->rowCount()) {
            $this->populate($query->fetch());
        }
    }
    public function populate($data) {
        if (empty($data['id'])) return false;
        $this->id = $data['id'];
        $this->list = $data['list'];
        $this->indb = true;
        $this->cat['value'] = $data['cat'];
        $this->cat['time'] = strtotime($data['cat_ts']);
        $this->so['value'] = intval($data['so']);
        $this->so['time'] = strtotime($data['so_ts']);
        $this->value['value'] = $data['val'];
        $this->value['time'] = strtotime($data['val_ts']);
        $this->qty['value'] = intval($data['qty']);
        $this->qty['time'] = strtotime($data['qty_ts']);
        $this->done['value'] = intval($data['done']);
        $this->done['time'] = strtotime($data['done_ts']);
    }
    public function updItem($item) {
        foreach(array("cat","so","value","done") as $key) {
            if ($this->{$key}['time'] < $item->{$key}->time) {
                if ($key == "so" || $key == "done") {
                    $this->{$key}['value'] = intval($item->{$key}->value);
                } else {
                    $this->{$key}['value'] = $item->{$key}->value;
                }

                $this->{$key}['time'] = $item->{$key}->time;
            }
        }
    }

    public function save() {
        if ($this->indb) {
            $sql = "
            UPDATE
              #schema#.items
            SET
              cat = :cat,
              so = :so,
              val = :val,
              qty = :qty,
              done = :done,
              cat_ts = :cat_ts,
              so_ts = :so_ts,
              val_ts = :val_ts,
              qty_ts = :qty_ts,
              done_ts = :done_ts
            WHERE
              id = :uuid
            ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(
                ":cat"=>$this->cat['value'],
                ":so"=>$this->so['value'],
                ":val"=>$this->value['value'],
                ":qty"=>$this->qty['value'],
                ":done"=>$this->done['value'],
                ":cat_ts"=>$this->timeFormat($this->cat['time']),
                ":so_ts"=>$this->timeFormat($this->so['time']),
                ":val_ts"=>$this->timeFormat($this->value['time']),
                ":qty_ts"=>$this->timeFormat($this->qty['time']),
                ":done_ts"=>$this->timeFormat($this->done['time']),
                ":uuid"=>$this->id,
            ));
        } else {
            $sql = "
                INSERT INTO
                  #schema#.items (
                    id,
                    list,
                    cat,
                    so,
                    val,
                    qty,
                    done,
                    cat_ts,
                    so_ts,
                    val_ts,
                    qty_ts,
                    done_ts
                  ) VALUES (
                    :id,
                    :list,
                    :cat,
                    :so,
                    :val,
                    :qty,
                    :done,
                    :cat_ts,
                    :so_ts,
                    :val_ts,
                    :qty_ts,
                    :done_ts
                  )
                ";
            $query = $GLOBALS['db']->prepare($sql);
            if ($query->execute(array(
                ":id"=>$this->id,
                ":list"=>$this->list,
                ":cat"=>$this->getValue("cat"),
                ":so"=>$this->getValue("so"),
                ":val"=>$this->getValue("value"),
                ":qty"=>$this->getValue("qty"),
                ":done"=>$this->getValue("done"),
                ":cat_ts"=>$this->getTime("cat"),
                ":so_ts"=>$this->getTime("so"),
                ":val_ts"=>$this->getTime("value"),
                ":qty_ts"=>$this->getTime("qty"),
                ":done_ts"=>$this->getTime("done"),
            ))) {
                // If query succeeded
                $this->indb = true;
            }
        }
    }

    public function toJSON ($json=false) {
        $tojson = array(
            "id" => $this->id,
            "cat" => $this->cat,
            "so" => $this->so,
            "value" => $this->value,
            "qty" => $this->qty,
            "done" => $this->done,
        );
        if ($json)
            return json_encode($tojson,JSON_HEX_QUOT);
        else
            return $tojson;
    }

    protected function timeFormat($ts) {
        if (empty($ts)) $ts = time();
        return date("Y-m-d H:i:s",$ts);
    }

}
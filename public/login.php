<?php
DEFINE("ENTRY","login");

try {
    $error = "";
    if (!empty($_GET['template']) && $_GET['template'] == "logout") {
        $token = "logout";
        setcookie("userToken", "", time() - 3600, "/");
        $template = "login";
    }
    require_once "../include/initDisplay.php";

    // Default template
    $template = "login";
    if (!empty($_GET['template']) && in_array($_GET['template'],array("login","register","validate","recover"))) {
        $template = $_GET['template'];
    }


    if (!empty($_POST['reg_login']) && isEmail($_POST['reg_login']) && !empty($_POST['reg_code']) && !empty($_POST['reg_passwd'])) {
        // Registration call, this will always exit at the end of this block if successful.

        // Register the user.
        $error = user::register($_POST['reg_login'],$_POST['reg_code'],$_POST['reg_passwd'],$_POST['reg_check']);
        if (empty($error)) { // Registration successful

            // Log the user in
            $user->login($_POST['reg_login'],$_POST['reg_passwd']);
            $user->setCookie();

            // Redirect
            header("Location: //".$_SERVER["HTTP_HOST"].linkTo("/"));
            exit();
        }
        // If registration failed, continue on with $error
    } elseif ($template == "recover") {
        if (!empty($_POST['recoverEmail'])) { // Send recover mail
            $error = user::recover($_POST['recoverEmail']);
        } else if (!empty($_POST['code']) && !empty($_POST['passwd'])) { // Change password
            if ($_POST['passwd'] == $_POST['check']) {
                $error = $user->newPasswd($_POST['passwd'],$_POST['code']);
            }
            error_log("Error: ".$error);
            if (empty($error)) {
                $user->setCookie();
                // Redirect
                header("Location: //".$_SERVER["HTTP_HOST"].linkTo("/"));
                exit();
            }
        } else if (!empty($_REQUEST['code'])) { // Display password form
            $smarty->assign("code",$_REQUEST['code']);
            $template = "newpass";
        }
    } elseif ($template == "validate") {
        if (!empty($_POST['reg_login'])) {
            // Register the user.
            $error = user::createSecret($_POST['reg_login'],$_POST['reg_name']);
        }
        $smarty->assign("reg_login",(empty($_REQUEST['reg_login']))?"":$_REQUEST['reg_login']);
        $smarty->assign("reg_code",(empty($_REQUEST['reg_code']))?"":$_REQUEST['reg_code']);

        if (!empty($error)) {
            $template = "register";
        }
    }

    $smarty->assign("error",$error);
    $smarty->assign("template",$template);
    $smarty->assign("login",(empty($_COOKIE['userLogin']))?"":$_COOKIE['userLogin']);

    $smarty->display("main.tpl");
} catch (Exception $e) {
    genericErrorHandler($e);
}

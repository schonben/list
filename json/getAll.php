<?php
if (!defined("ENTRY"))
    exit("Invalid entry point");

$lists = array();

foreach (ulist::lists($user->getId(),true) as $list) {
    $lists[] = $list->toJSON(false);
}

$output['action'] = "index";
$output['data'] = $lists;
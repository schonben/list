<script src="{'/js/list.js'|linkTo}" defer="defer"></script>
<script src="{'/js/index.js'|linkTo}" defer="defer"></script>
<script>
    var jsonURL = "{$setting.jsonURL}";
    var userRefresh = {$user->getRefresh(true)};
    var rpath = "{'/'|linkTo}";

    var interval = setInterval(function() { // Periodically call syncJSON
        syncJSON();
    },(userRefresh));
</script>

<!-- Sync status icon, syncs on click -->
<div onclick="syncJSON()" id="sync" class="sync"></div>

<!-- Index section -->
<section class="main" id="index">
    <ul class="lists">
    </ul>
    <form action="{'/addList.do'|linkTo}" method="post">
        <button type="button" onclick="showAddFields();" id="addbutton">+</button>
        <input type="text" class="hidden" id="newlist" name="newlist" placeholder="List Name"/>
        <input type="submit" class="hidden" id="newbutton" value="Add"/>
    </form>
</section>

<!-- List section (Hidden) -->
<section class="main hidden" id="list">
    <a id="back" href="#" onclick="closeList();">
        <img src="{'/img/back.png'|linkTo}" alt="Back"/>
    </a>

    <h2 id="listname"></h2>

    <!-- Form is never submitted -->
    <form>
        <input type="hidden" id="listid" name="id" value=""/>
        <table class="list">
            <thead>
            <tr>
                <td colspan="2" id="newValueCell"><input type="text" id="new_value" placeholder="Text" name="value" value=""/></td>
                <td colspan="2" id="newCatCell"><input type="text" id="new_cat" placeholder="Category" name="cat" value=""/></td>
            </tr>
            </thead>
            <tbody id="listbody">
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    <img src="{'/img/totrash.png'|linkTo}" class="delchecked" onclick="delChecked();" alt="Delete checked" title="Delete Checked"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</section>
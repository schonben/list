<?php
define("ENTRY", "db");

include "../include/init.php";

// Inserting some test data into the db

$sql = "
    INSERT INTO users (
            email,
            fname,
            lname,
            passwd
        ) VALUES (
            'test@test.com',
            'Test',
            'User',
            ''
        )
    ";
//$query = $GLOBALS['db']->prepare($sql);
//$query->execute();

$sql = "
    INSERT INTO lists (
            listname,
            description
        ) VALUES (
            'Testing',
            'Testing'
        )
    ";
$query = $GLOBALS['db']->prepare($sql);
$query->execute();

$sql = "
    SELECT
        id
    FROM
        lists
    ";
$query = $GLOBALS['db']->prepare($sql);
$query->execute();
$row = $query->fetch();

$sql = "
    INSERT INTO items (
            list,
            cat,
            so,
            value
        ) VALUES (
            :list,
            :cat,
            :so,
            :value
        )
    ";
$query = $GLOBALS['db']->prepare($sql);
$so=0;
$query->execute(array(
    ":list"=>$row['id'],
    ":cat"=>"Testing",
    ":so"=>$so++,
    ":value"=>"Test ".$so++,
    ));
$query->execute(array(
    ":list"=>$row['id'],
    ":cat"=>"Testing",
    ":so"=>$so++,
    ":value"=>"Test ".$so++,
    ));
$query->execute(array(
    ":list"=>$row['id'],
    ":cat"=>"Testing",
    ":so"=>$so++,
    ":value"=>"Test ".$so++,
    ));
$query->execute(array(
    ":list"=>$row['id'],
    ":cat"=>"Testing",
    ":so"=>$so++,
    ":value"=>"Test ".$so++,
    ));
$query->execute(array(
    ":list"=>$row['id'],
    ":cat"=>"Testing",
    ":so"=>$so++,
    ":value"=>"Test ".$so++,
    ));

<section class="main">
    <form action="{'/'|linkTo}" method="post">
        <label for="login">E-mail:</label><br/>
        <input id="login" type="email" name="login" value="{$login}"/><br/>
        <label for="passwd">Password:</label><br/>
        <input id="passwd" type="password" name="passwd"/><br/>
        <button type="submit">Login</button>
        <label for="session">Session length:</label>
        <select id="session" name="sl">
            <option value="120">2 Minutes</option>
            <option value="600">10 Minutes</option>
            <option value="3600" selected="selected">1 Hour</option>
            <option value="86400">1 Day</option>
            <option value="604800">1 Week</option>
            <option value="2592000">30 Days</option>
        </select><br/>
    </form>
    <br/>

    <div>
        <a href="{'/register'|linkTo}">Register</a> a new user.<br/>
        <a href="{'/recover'|linkTo}">Recover</a> lost password.
    </div>
</section>
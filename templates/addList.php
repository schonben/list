<?php
if (!defined("ENTRY") || ENTRY != "post")
exit("Invalid entry point");

$list = new ulist();
$list->create($_POST['newlist']);
$list->addAccess($user->getId(),0);
$list->save();

$returl = "#list/".$list->getId();

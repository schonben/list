<?php
DEFINE("ENTRY","index");

try {
    require_once "../include/initDisplay.php";

    if (file_exists($setting['apath']."/templates/{$template}.php")) {
        include $setting['apath']."/templates/{$template}.php";
    }
    if (!$smarty->templateExists("{$template}.tpl")) {
        $template = "";
    }

    $smarty->assign("template",$template);
    $smarty->assign("id",$id);
    $smarty->display("main.tpl");
} catch (Exception $e) {
    genericErrorHandler($e);
}
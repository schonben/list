<script type="text/javascript" src="{'/js/password.js'|linkTo}"></script>
<script type="text/javascript">
    function chPasswd() {
        switch (checkPass($('#passwd').val(),$('#check').val(),10)) {
            case 0:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("ok");
                break;
            case 1:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("pass");
                break;
            case 2:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("shallow");
                break;
            default:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("fail");
                break;
        }
    }
    function validPass() {
        var ret = isValidPass($('#passwd').val(),$('#check').val(),10);
        if (ret == 1) {
            alert("Passwords do not match.");
            return false;
        }
        if (ret == 2) {
            alert("Password is too short.");
            return false;
        }
        return true;
    }
</script>

<section class="main">

    {if $error != ""}
        <div class="error">
            {$error|ucfirst}
        </div>
    {/if}

    <form action="{'/recover'|linkTo}" method="post">
        <input type="hidden" name="code" value="{$code}"/>
        <label for="passwd">New Password:</label><br/>
        <div class="pass" id="passCheck"></div>
        <input id="passwd" type="password" name="passwd" onkeyup="chPasswd()" value=""/><br/>
        <input id="check" type="password" name="check" onkeyup="chPasswd()" value=""/><br/>
        <button type="submit" onclick="return validPass();">Change</button><br/>
        <hr/>
        <p>Note that the link you used are not validated until you submit this form, so if you are not logged in after this check your link.</p>
    </form>
    <br/>
</section>
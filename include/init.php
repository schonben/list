<?php
if (!defined("ENTRY"))
    exit("Invalid entry point");

include_once "localFunctions.php";

mb_language("uni");
mb_internal_encoding("UTF-8");
spl_autoload_register('classAutoLoader',true,true);

/* Will try this later when I can edit online
if (!file_exists(getcwd()."/../setting.php")) {
    echo "File: settings.php does not exist.";
    exit();   
}
*/

//include "uuid.inc";

include "../settings.php";

set_error_handler("phpErrorHandler");

require_once "foondb/db.class.php";
require_once "foonuser/user.class.php";

$dsn = "{$setting['db']['type']}:dbname={$setting['db']['name']};host={$setting['db']['server']}";
$GLOBALS['db'] = new db($dsn,$setting['db']['user'],$setting['db']['password']);
$GLOBALS['db']->schema($setting['schema'],true);

// This application will not use php sessions. It implements it's own session management with it's own cookie token.
// Keep in mind that $token can be defined before calling init.php
if (!isset($token)) {
    // In case $token is not defined.
    $token = null;
}
if (empty($token) && !empty($_COOKIE['userToken'])) {
    $token = $_COOKIE['userToken'];
}
$user = new user($token);
if (isset($_POST['login']) && isset($_POST['passwd'])) {
    // Attempt a password login
    if (!$user->login($_POST['login'],$_POST['passwd'],$_POST['sl'])) {
        // Handle failed login
        header("Location: ".linkTo("/login?message=Login+Failed"));
        exit();
    }
}

// Check if user is logged in
if (!in_array(ENTRY,array("login","db","json")) && !$user->getId()) {
    header("Location: ".linkTo("/login"));
    exit();
}

$template = "";
if (!empty($_GET['template'])) {
    $template = $_GET['template'];
}

if (empty($template) || !ctype_alnum($template)) {
    $template = "index";
}

$id = "";
if (!empty($_GET['id'])) {
    $id = $_GET['id'];
}

// Static stuff
$modes = array(
    0 => "Owner",
    2 => "Can Edit",
    4 => "Can Use",
    6 => "Can View",
);

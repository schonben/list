<?php

class uList {
    protected $id;
    protected $name;
    protected $description;
    protected $showCat;
    protected $showQty;

    protected $items = array();
    protected $access = array();

    protected $query;

    public function __construct($id="") {
        $sql = "
            SELECT
              *
            FROM
              #schema#.lists
            WHERE
              id = :uuid
            ";
        $this->query = $GLOBALS['db']->prepare($sql);
        if (!empty($id)) {
            $this->retrieve($id);
        }
    }

    // Getters
    public function getId() {
        if (empty($this->id)) return false;
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }

    public function getItems() {
        return $this->items;
    }
    public function getItemCount($status="all") {
        $count = 0;
        foreach ($this->items as $key => $item) {
            if (($status == "all" && $item->getDone() < 2) || $item->getDone() == $status) {
                $count++;
            }
        }
        return $count;
    }

    public function getCategories() {
        $cat = array();
        foreach ($this->items as $item) {
            if (!in_array($item->getCat(),$cat)) {
                $cat[] = $item->getCat();
            }
        }
        return $cat;
    }
    public function getAccess($userId,$mode=false) {
        if ($mode === false) {
            if (isset($this->access[$userId])) return $this->access[$userId];
            return 10;
        } else {
            if (isset($this->access[$userId])) {
                return ($this->access[$userId] <= $mode);
            }
            return false;
        }
    }
    public function getUsers() {
        return $this->access;
    }
    public function getShow($type,$ret="",$alt="") {
        $value = false;
        switch (strtolower($type)) {
            case "cat":
            case "category":
                if (!empty($this->showCat)) $value = true;
                break;
            case "qty":
            case "quantity":
            if (!empty($this->showQty)) $value = true;
                break;
        }
        if (empty($ret)) {
            return $value;
        } else {
            if ($value) {
                return $ret;
            }
            return $alt;
        }
    }

    // Setters
    public function setName($name) {
        $this->name = $name;
    }

    public function setShow($type,$value) {
        switch (strtolower($type)) {
            case "cat":
            case "category":
                $this->showCat = empty($value)?false:true;
                break;
            case "qty":
            case "quantity":
                $this->showQty = empty($value)?false:true;
                break;
        }
    }

    // Fetches the list from the database
    public function retrieve ($id) {
        $this->query->execute(array(":uuid"=>$id));
        $row = $this->query->fetch();

        // Quit if not found
        if (empty($row['id']))
            return false;

        $this->id = $row['id'];
        $this->name = $row['listname'];
        $this->description = $row['description'];

        $this->showCat = $row['showcat'];
        $this->showQty = $row['showqty'];

        $this->populateItems();
        $this->populateAccess();

        return true; // If we got this far
    }
    public function create($name) {
        $uuid = uuid::getUUID();
        $sql = "
            INSERT INTO
              #schema#.lists (
                id,
                listname
              ) VALUES (
                :uuid,
                :listname
              )";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(
            ":uuid"=>$uuid,
            ":listname"=>$name,
        ));
        $this->retrieve($uuid);
    }
    public function populateItems() {
        $sql = "
            SELECT
              *
            FROM
              #schema#.items
            WHERE
              list = :list
            ORDER BY
              cat,
              so
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":list"=>$this->id));
        $this->items = array();
        while ($row = $query->fetch()) {
            $this->items[$row['id']] = new uitem($row);
        }
    }
    public function populateAccess() {
        $sql = "
            SELECT
              *
            FROM
              #schema#.listuser
            WHERE
              listid = :list
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":list"=>$this->id));
        $this->access = array();
        while ($row = $query->fetch()) {
            $this->access[$row['userid']] = $row['mode'];
        }
    }
    public function addAccess ($user,$mode) {
        $this->access[$user] = $mode;
    }
    public function delAccess ($user) {
        unset($this->access[$user]);
    }

    public function syncItems($items) {
        foreach ($items as $key => $item) {
            if (!empty($item->id) && isset($this->items[$item->id])) {
                // Update existing
                $this->items[$item->id]->updItem($item);
            } else {
                // Add new item
                $newitem = new uitem($item);
                $newitem->setList($this->id);

                // Add the item to the items array.
                $this->items[] = $newitem;
            }
        }
        $this->save();
    }

    public function save() {
        $sql = "
            UPDATE
              #schema#.lists
            SET
              listname = :listname,
              description = :description,
              showcat = :showcat,
              showqty = :showqty
            WHERE
              id = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);

        $query->execute(array(
            ":listname" => $this->name,
            ":description" => $this->description,
            ":showcat" => $this->getShow("cat",1,0),
            ":showqty" => $this->getShow("qty",1,0),
            ":uuid" => $this->id,
        ));

        // Save Items
        $this->saveItems();

        //Save Access rights
        $this->saveAccess();
    }

    protected function saveItems () {
        foreach ($this->items as &$item) {
            $item->save();
        }
    }

    protected function saveAccess () {
        // Prepare the queries
        $sql = "
            SELECT
              *
            FROM
              #schema#.listuser
            WHERE
              listid = :listid
            AND
              userid = :userid
            ";
        $check = $GLOBALS['db']->prepare($sql);
        $sql = "
            UPDATE
              #schema#.listuser
            SET
              mode = :mode
            WHERE
              listid = :listid
            AND
              userid = :userid
            ";
        $update = $GLOBALS['db']->prepare($sql);
        $sql = "
            INSERT INTO
              #schema#.listuser (
                listid,
                userid,
                mode
              ) VALUES (
                :listid,
                :userid,
                :mode
              )
            ";
        $insert = $GLOBALS['db']->prepare($sql);

        $args = array($this->id);
        foreach ($this->access as $id => $mode) {
            $args[] = $id;
            $check->execute(array(":listid"=>$this->id,":userid"=>$id));
            $data = array(":listid"=>$this->id,":userid"=>$id,":mode"=>$mode);
            if ($check->rowCount()) {
                // Update record
                $update->execute($data);
            } else {
                // Create record
                $insert->execute($data);
            }
        }
        $asql = "";
        if (count($this->access)) {
            $qMarks = trim(str_repeat('?,', count($this->access)),",");
            $asql = "
                AND
                  userid NOT IN ({$qMarks})
                ";
        }
        $sql = "
            DELETE
            FROM
              #schema#.listuser
            WHERE
              listid = ?
            ".$asql;
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute($args);
    }

    public function toJSON ($json=true) {
        $items = array();
        foreach ($this->items as $item) {
            $items[] = $item->toJSON(false);
        }

        $tojson = array(
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "showCat" => $this->showCat,
            "showQty" => $this->showQty,
            "items" => $items,
            );
        if ($json)
            return json_encode($tojson,JSON_HEX_QUOT);
        else
            return $tojson;
    }

    public function rm () {
        // Delete the list and all things connected to it.
        $sql = "
            DELETE
            FROM
              #schema#.listuser
            WHERE
              listid = :listid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":listid" => $this->id));
        $sql = "
            DELETE
            FROM
              #schema#.items
            WHERE
              list = :listid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":listid" => $this->id));
        $sql = "
            DELETE
            FROM
              #schema#.lists
            WHERE
              id = :listid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":listid" => $this->id));
    }

    // Fetches an array containing all the lists that a user has access to
    public static function lists ($userId,$object=false) {
        // TODO add user access thingy
        $sql = "
            SELECT
              li.id,
              li.listname
            FROM
              #schema#.lists as li
            JOIN
              #schema#.listuser as lu
            ON
              li.id = lu.listid
            WHERE
              lu.userid = :userid
            ORDER BY
              listname
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":userid" => $userId));

        $returnarray = array();
        while ($row = $query->fetch()) {
            if ($object) {
                $returnarray[$row['id']] = new ulist($row['id']);
            } else {
                $returnarray[$row['id']] = $row['listname'];
            }

        }
        return $returnarray;
    }

}
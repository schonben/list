<?php
DEFINE("ENTRY","post");

try {
    require_once "../include/init.php";
    $returl = "";

    if (ctype_alnum($_GET['template']) && file_exists($setting['apath']."/templates/{$_GET['template']}.php")) {
        $GLOBALS['db']->beginTransaction();

        include $setting['apath']."/templates/{$_GET['template']}.php";

        if ($db->isFail()) {
            $db->rollBack();
        } else {
            $db->commit();
        }
    } else {
        error_log("Invalid template: {$template}");
    }

    header("Location: ".linkTo($returl));
} catch (Exception $e) {
    genericErrorHandler($e);
}
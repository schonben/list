<section class="main">
    <div>
        Register a new user.
    </div>

    {if $error != ""}
        <div class="error">
            {$error|ucfirst}
        </div>
    {/if}

    <form action="{'/validate'|linkTo}" method="post">
        <label for="login">E-mail:</label><br/>
        <input id="login" type="email" name="reg_login" value=""/><br/>
        <label for="name">Name:</label><br/>
        <input id="name" type="text" name="reg_name" value=""/><br/>
        <button type="submit">Validate e-mail</button><br/>
    </form>
</section>
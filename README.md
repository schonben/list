list
====

HTML5 list application

This application uses localStorage to store the list data. It syncs the data with the server when there is a net connection, but keeps data up to date offline as well.

<section class="main">
    <h2>Page not available</h2>
    <p>This page is not available in offline mode. Please reconnect you browser and reload the page.</p>
    <p>Or <a href="/">return to the index page</a>.</p>
</section>
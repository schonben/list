<script type="text/javascript" src="{'/js/password.js'|linkTo}"></script>
<script type="text/javascript" src="{'/js/settings.js'|linkTo}"></script>
<script type="text/javascript">
    function chPasswd() {
        switch (checkPass($('#passwd1').val(),$('#passwd2').val(),10)) {
            case 0:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("ok");
                break;
            case 1:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("pass");
                break;
            case 2:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("shallow");
                break;
            default:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("fail");
                break;
        }
    }
</script>

<section class="main">
    {assign var="back" value="/#`$st.template`/`$st.id`"}
    <a id="back" href="{$back|linkTo}">
        <img src="{'/img/back.png'|linkTo}" alt="Back"/>
    </a>

    <form id="settings" class="edit" action="{'/saveSettings.do'|linkTo}" method="post">
        <input type="submit" id="save" name="save" value="Save"/>
        <h2>Settings</h2>
        <hr/>
        <input type="hidden" name="id" value="{$user->getId()}"/>
        <input type="hidden" name="st_temp" value="{$st.template}"/>
        <input type="hidden" name="st_id" value="{$st.id}"/>
        <label for="name">Name</label>
        <input id="name" type="text" name="fname" value="{$user->getFirstName()}" size="10"/>
        <input id="name" type="text" name="lname" value="{$user->getLastName()}" size="15"/><br/>
        <label for="refresh">Refresh</label>
        <input type="range" id="refresh" oninput="$('#showRefresh').html($('#refresh').val());" name="refresh" min="5" step="5" max="300" value="{$user->getRefresh()}"/>
        <span id="showRefresh">{$user->getRefresh()}</span>s.<br/>
        <hr/>
        <p>Note: Updating the password logs out all sessions, even if you use the same password as before.</p>
        <label for="passwd">Password</label>
        <input id="passwd" type="password" name="passwd" size="10"/><br/>
        <div class="pass" id="passCheck"></div>
        <label for="passwd1">New Password</label>
        <input id="passwd1" type="password" name="passwd1" size="10" onkeyup="chPasswd()"/>
        <input id="passwd2" type="password" name="passwd2" size="10" onkeyup="chPasswd()"/><br/>
        {if $st.template == "list"}
            <hr/>
            <script type="text/javascript">
                var modes = {$modes|json_encode}
                var access = {$list->getUsers()|json_encode}
                var users = {$users|json_encode}
                $(function(){
                    $("#newmode").val(4);
                    drawUsers();
                });
            </script>
            <label for="list">List Name</label>
            <input id="list" class="big" type="text" name="listname" value="{$list->getName()}"/><br/>
            <input id="showcat" type="checkbox" name="showcat" value="true" {$list->getShow('cat',' checked="checked"')} />
            <label for="showcat">Show Categories</label><br/>
            <input id="showqty" type="checkbox" name="showqty" value="true" {$list->getShow('qty',' checked="checked"')}/>
            <label for="showqty">Show Quantity</label><br/>
            <hr/>
            <table style="width: 100%;">
                <tbody id="userlist">
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <select id="newuser" name="newuser">
                                <option value="">-- New User --</option>
                                {foreach $users as $id => $usr}
                                    <option value="{$id}">{$usr.fname} {$usr.lname}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <select id="newmode" name="newmode">
                                {foreach $modes as $i => $val}
                                    <option value="{$i}">{$val}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td>
                            <button type="button" onclick="addUser(this.form.newuser.value,this.form.newmode.value);">Add</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <hr/>
            <input type="submit" id="delete" onclick="if (confirm('Are you sure?')) deleteList('{$st.id}'); else return false;" name="delete" value="Delete"/>
        {/if}
        <br style="clear: both;"/>
    </form>
</section>
<script type="text/javascript" src="{'/js/password.js'|linkTo}"></script>
<script type="text/javascript">
    function chPasswd() {
        switch (checkPass($('#passwd').val(),$('#check').val(),10)) {
            case 0:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("ok");
                break;
            case 1:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("pass");
                break;
            case 2:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("shallow");
                break;
            default:
                $("#passCheck").removeClass();
                $("#passCheck").addClass("fail");
                break;
        }
    }
    function validPass() {
        var ret = isValidPass($('#passwd').val(),$('#check').val(),10);
        if (ret == 1) {
            alert("Passwords do not match.");
            return false;
        }
        if (ret == 2) {
            alert("Password is too short.");
            return false;
        }
        return true;
    }
</script>


<section class="main">
    <div>
        Register a new user.
    </div>

    {if $error != ""}
        <div class="error">
            {$error|ucfirst}
        </div>
    {/if}

    <form action="{'/validate'|linkTo}" method="post">
        <input type="hidden" name="reg_login" value="{$reg_login}"/>
        <label for="code">Code:</label><br/>
        <input id="code" type="text" name="reg_code" value="{$reg_code}"/><br/>
        <label for="passwd">Password:</label><br/>
        <div class="pass" id="passCheck"></div>
        <input id="passwd" type="password" name="reg_passwd" onkeyup="chPasswd()" value=""/><br/>
        <input id="check" type="password" name="reg_check" onkeyup="chPasswd()" value=""/><br/>
        <button type="submit" onclick="return validPass();">Register</button><br/>
    </form>
</section>

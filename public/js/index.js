$(function (){
    $("#listbody").sortable({ // Make the list sortable
        handle: '.handle',
        helper: fixHelper,
        stop: function( event, ui ) { saveOrder(ui); }
    });
    $("#new_value").autocomplete({
        source: itemList
    });
    $("#new_cat").autocomplete({
        source: categoryList
    });
    $("#new_value").on("change",function () {
        addItem();
    });
    $("#new_value").on("autocompleteselect",function (event,ui) {
        addItem(ui.item.value);
        return false; // Stop the autocomplete from populating the field after object is added.
    });

    if (window.location.hash.substr(0,6) == "#list/") {
        var hashList = window.location.hash.substr(6);
        openList(hashList);
    } else {
        syncJSON();
        buildIndex();
    }
});

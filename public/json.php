<?php
DEFINE("ENTRY","json");
/* Index file for json calls to the server. */

$token = urldecode($_POST['token']);
require_once "../include/init.php";

/*
 * Error codes:
 * 1 - User not logged in (Invalid Token)
 * 2 - Malformed input (json)
 * 3 - Invalid action (action doesn't exist).
 */

$output = array(
    "status" => 0, // 0 = success, other = error. 1-15 reserved for "system" errors, 16+ = action specific.
    "errmsg" => "", // This should be used only for debugging. No functionality should depend on this.
    "token" => $user->getToken(), // Return a fresh token.
    "action" => "", // What should the be done?
    "data" => "", // Data to do it with.
    );

try {
    if (!$user->getId()) {
        // User not logged in
        $output['status'] = 1;
        $output['errmsg'] = "No user id, check your token";
    } elseif ($json = json_decode($_POST['json'])) {
        if (!empty($_POST['action']) && ctype_alnum($_POST['action']) && file_exists($setting['apath']."/json/{$_POST['action']}.php")) {
            include "../json/{$_POST['action']}.php";
        } else {
            // Invalid action
            $output['status'] = 3;
            $output['errmsg'] = "Invalid action";
        }
    } else {
        // Malformed input
        $output['status'] = 2;
        $output['errmsg'] = "Malformed input";
    }
} catch (Exception $e) {
    $output['status'] = 5;
    $output['errmsg'] = "PHP Error: ".$e->getMessage();
}

if ($GLOBALS['setting']['dev']) { // Simulate slow connection
    sleep(4);
}

echo json_encode($output,JSON_HEX_QUOT);

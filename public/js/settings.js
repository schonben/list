function drawUsers () {
    $("#userlist").html("");
    $.each(access,function(id, mode){
        var options = "";
        $.each(modes,function(index, value){
            options += '<option value="'+index+'"'+(mode == index?' selected="selected"':'')+'>'+value+'</option>';
        });
        $("#userlist").append('<tr id="' + id + '">' +
            '<td>' +
            users[id].fname + ' ' + users[id].lname +
            '</td>' +
            '<td>' +
            '<select onchange="updateMode(\'' + id + '\',this.value)" name="mode[' + id + ']">' +
            options +
            '</select>' +
            '</td>' +
            '<td>' +
            '<button type="button" onclick="removeUser(\'' + id + '\');">Remove</button>' +
            '</td>' +
            '</tr>');
    });
}
function addUser (id,mode) {
    if (id.length > 0) {
        access[id] = mode;
    }
    $("#newuser").val("");
    $("#newmode").val(4);
    drawUsers();
}
function removeUser(id) {
    delete access[id];
    drawUsers();
}
function updateMode(id,mode) {
    access[id] = mode;
}

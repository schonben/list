<section class="main">

    {if $error != ""}
        <div class="error">
            {$error|ucfirst}
        </div>
    {/if}

    <form action="{'/recover'|linkTo}" method="post">
        <label for="login">E-mail:</label><br/>
        <input id="login" type="email" name="recoverEmail" value="{$login}"/><br/>
        <button type="submit">Recover</button>
    </form>
    <br/>
</section>
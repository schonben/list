<?php
if (!defined("ENTRY"))
    exit("Invalid entry point");

require_once "init.php";
$user->setCookie();

require_once "Smarty-3.1.19/libs/Smarty.class.php";

// Create new smarty object
$smarty = new Smarty();

$smarty->muteExpectedErrors();

// Define the standard smarty dirs
$smarty->setCompileDir($setting['apath']."/smarty/template_c");
$smarty->setCacheDir($setting['apath']."/smarty/cache");
$smarty->setConfigDir($setting['apath']."/smarty/config");

// Search path for templates
$smarty->setTemplateDir(array($setting['apath']."/templates"));

// Assign some standard stuff to smarty
$smarty->assign("setting",$setting);
$smarty->assign("user",$user);
$smarty->assign("modes",$modes);


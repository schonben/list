<?php

// Note default string values must be quoted.

// Table definitions
$tables = array(
    array(
        "type" => 3, // 1 - temporary (re-created each time), 2 - May delete columns, 3 - rename columns, 4 - do not delete
        "name" => "users", // Name of table
        "pkey" => "id", // Primary key column(s) (comma separated)
        "cols" => array( // List the columns
            "id" => array("type"=>"uuid","default"=>"public.uuid_generate_v4()"),
            "email" => array("type"=>"character varying"),
            "fname" => array("type"=>"character varying"),
            "lname" => array("type"=>"character varying"),
            "passwd" => array("type"=>"character varying"),
            "refresh" => array("type"=>"integer","default" => 30),
        ),
        "index" => array( // List of indexes
            "indexname" => array("table"=>"tablename","definition"=>"count(foo)"),
        ),
    ),
    array(
        "type" => 1, // 1 - temporary (re-created each time), 2 - May delete columns, 3 - rename columns, 4 - do not delete
        "name" => "emailsecret", // Name of table
        "pkey" => "email", // Primary key column(s) (comma separated)
        "cols" => array(
            "email" => array("type"=>"character varying"),
            "name" => array("type"=>"character varying"),
            "secret" => array("type"=>"character varying"),
            "ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
        ),
    ),
    array(
        "type" => 3, // 1 - temporary (re-created each time), 2 - May delete columns, 3 - rename columns, 4 - do not delete
        "name" => "lists", // Name of table
        "pkey" => "id", // Primary key column(s) (comma separated)
        "cols" => array(
            "id" => array("type"=>"uuid","default"=>"public.uuid_generate_v4()"),
            "listname" => array("type"=>"character varying"),
            "showcat" => array("type"=>"boolean","default"=>"true"),
            "showqty" => array("type"=>"boolean","default"=>"false"),
            "description" => array("type"=>"character varying"),
        ),
    ),
    array(
        "type" => 3, // 1 - temporary (re-created each time), 2 - May delete columns, 3 - rename columns, 4 - do not delete
        "name" => "listuser", // Name of table
        "pkey" => "listid,userid", // Primary key column(s) (comma separated)
        "cols" => array(
            "listid" => array("type"=>"uuid"),
            "userid" => array("type"=>"uuid"),
            "mode" => array("type"=>"integer","default"=>0),
            "description" => array("type"=>"character varying"),
        ),
    ),
    array(
        "type" => 3, // 1 - temporary (re-created each time), 2 - May delete columns, 3 - rename columns, 4 - do not delete
        "name" => "items", // Name of table
        "pkey" => "id", // Primary key column(s) (comma separated)
        "cols" => array(
            "id" => array("type"=>"uuid","default"=>"public.uuid_generate_v4()"),
            "list" => array("type"=>"uuid"),
            "created" => array("type"=>"uuid"),
            "created_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
            "modified" => array("type"=>"uuid"),
            "cat" => array("type"=>"character varying"),
            "so" => array("type"=>"integer"),
            "val" => array("type"=>"character varying"),
            "qty" => array("type"=>"int","default"=>"1"),
            "done" => array("type"=>"int","default"=>"0"),
            "cat_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
            "so_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
            "val_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
            "qty_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
            "done_ts" => array("type"=>"timestamp with time zone","default"=>"now()"),
        ),
    ),
);


<?php
if (!defined("ENTRY") || ENTRY != "post")
    exit("Invalid entry point");

if ($_POST['id'] != $user->getId()) {
    error_log("Wrong User");
    return;
}

// Update user data
$user->setFirstName($_POST['fname']);
$user->setLastName($_POST['lname']);
$user->setRefresh($_POST['refresh']);
// Save the object
$user->save();

if (!empty($_POST['passwd']) && !empty($_POST['passwd1']) && !empty($_POST['passwd2']) && $_POST['passwd1'] == $_POST['passwd2']) {
    // Update the users password
    if (!$user->changePassword($_POST['passwd'],$_POST['passwd1'])) {
        // TODO handle failed password change.
    }
}

switch ($_POST['st_temp']) {
    case "list":
        $list = new ulist($_POST['st_id']);
        if ($list->getAccess($user->getId(),2)) { //Check that the user has access.
            if (empty($_POST['delete'])) { //Update list data
                $list->setName($_POST['listname']);
                $list->setShow("cat",empty($_POST['showcat'])?false:true);
                $list->setShow("qty",empty($_POST['showcat'])?false:true);
                $hasOwner = false;
                if (empty($_POST['mode']) || !is_array($_POST['mode'])) $_POST['mode'] = array();
                foreach ($_POST['mode'] as $usr => $mode) {
                    $list->addAccess($usr,$mode);
                    if ($mode == 0) {
                        $hasOwner = true;
                    }
                }
                foreach ($list->getUsers() as $usr => $mode) {
                    if (!isset($_POST['mode'][$usr])) {
                        $list->delAccess($usr);
                    }
                }
                if (!$hasOwner) {
                    $list->addAccess($user->getId(),0);
                }
                $list->save();
                $returl = "#list/".$list->getId();
            } else { //Delete the list
                $list->rm();
                unset($list);
                $returl = "";
            }
        }
        break;
}
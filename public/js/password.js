function checkPass(ps,ch,depth) {
    if (ch.length == 0 || ps == ch) {
        if (pwDepth(ps) < depth) {
            return (ps.length == 0) ? 1 : 2;
        } else if (ch.length == 0) {
            return 1;
        }
    } else {
        return 3;
    }
    return 0;
}
function isValidPass(ps,ch,depth) {
    if (pwDepth(ps) < depth) {
        return 2;
    }
    if (ps != ch) {
        return 1;
    }
    return 0;
}

function pwDepth (pass) {
    lc = false;
    uc = false;
    nr = false;
    sy = false;
    if (/[a-z]/.test(pass)) lc = true;
    if (/[A-Z]/.test(pass)) uc = true;
    if (/[0-9]/.test(pass)) nr = true;
    if (/[^a-zA-Z0-9]/.test(pass)) sy = true;
    bd = 0;
    if (lc) bd += 26;
    if (uc) bd += 26;
    if (nr) bd += 10;
    if (sy) bd += 33;

    return log10(Math.pow(bd,pass.length));
}
function log10(val) {
    return Math.log(val) / Math.LN10;
}